<?php
/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2019 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

$oTheme = oxNew('oxtheme');
$oTheme->load('wave');

/**
 * Theme Information
 */
$aTheme = array(
	'id'             => 'wavechild',
	'title'          => 'wavechild ("Wave" Child Theme)',
	'description'    => 'Develop - Child Theme of "WAVE", the official responsive OXID theme',
	'thumbnail'      => 'theme.png',
	'version'        => '1.0.0',
	'author'         => 'the-real-world.de (childtheme) OXID (parenttheme)',
	'parentTheme'    => 'wave',
	'parentVersions' => array($oTheme->getInfo('version'))
);