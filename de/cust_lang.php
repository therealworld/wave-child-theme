<?php
/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2019 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

$sLangName = "Deutsch";

$aLang = array(
	'charset'                                               => 'UTF-8'
);